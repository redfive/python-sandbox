                    7
            6               4
        1       0       -1      9

Ancestors 
Lowest Common Ancestor LCA(-1, 9) = 4
LCA(4, 9) = 4

class Node {
    Node *left;
    Node *right;
    int value;
};

// both a and b are present in the tree
// the values in the tree are all unique
// this is a generic binary tree and not a binary search tree

int LCA(Node *root, int a, int b) {
}

def LCA(node, valueA, valueB):
  ancestorsA = []
  ancestorsB = []

  # special case indentity
  if valueA == valueB:
    return valueA

  # find A's ancestors
  findValue( node, valueA, ancestorsA)

  # find B's ancestors
  findValue( node, valueB, ancestorsB)

  # find the shorter list
  if length(ancestorsA) < length(ancestorsB):
    loopLength = length(ancestorsA)
  else
    loopLength = length(ancestorsB)

  lcaNode = None
  for index in range(0, loopLength-1):
    if ancestorsA.get(index) == ancestorsB.get(index):
      lcaNode = ancestorsA.get(index)
    else:
      break

  return lcaNode.value

# returns the ancestor list to the node containing value
# ancestors is a list []
def findValue( node, value, ancestors ):
  ancestors.append(node)

  if node.value == value:
    return True

  if node.left:
    if findValue( node.left, value, ancestors ):
      return True

  if node.right:
    if findValue(node.right, value, ancestors ):
      return True

  ancestors.pop(node)
  return False




