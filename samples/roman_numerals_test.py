#!/usr/bin/env python

import unittest
from roman_numerals import RomanParser

class RomanTestCase(unittest.TestCase):
  '''
    Test the RomanParser class' ability to identiy Roman Numerals. This
    TestCase class only tests the isValidRomanNumeral() method. See
    RomanCovertTestCase below for testing of conversion to decimal.
  '''

  # test input that should all properly parse as Roman numerals
  posOnesList = [ 'i', 'ii', 'iii', 'iv', 'v', 'vi', 'vii', 'viii', 'ix' ]
  posTensList = [ 'x', 'xx', 'xxx', 'xl', 'l', 'lx', 'lxx', 'lxxx', 'xc' ]
  posHundredsList = [ 'c', 'cc', 'ccc', 'cd', 'd', 'dc', 'dcc', 'dccc', 'cm' ]
  posThousandsList = [ 'm', 'mm', 'mmm' ]
  posRandomList = [ 'xiv', 'cxl', 'mmmdccclxxxviii', 'mmmcmxcix']
  posWhitespaceList = [ ' mm', 'mm ', ' mm ' ]
  posCapitalsList = [ 'I', 'V', 'X', 'L', 'C', 'D', 'M' ]
  posMixedCapitalsList = [ 'iI', 'Xl', 'DcC', 'mMm', 'MmmDCcCLxXxVIii' ]

  # test input that should not properly parse as Roman numerals
  negAlphaList = [ 'a', 'b', 'e', 'f', 'g', 'h', 'j', 'k', 'n', 'o', 'p', 'q',
                   'r', 's', 't', 'u', 'w', 'y', 'z']
  negEmptyList = [ '', ' ', None ]
  negRandomList = [ 'ixv', 'xcl', 'mmmm', 'cccc', 'dd', 'vv', 'll']
  negWhitespaceList = [ ' m m', 'm m ', ' m m ' ]

  # get the parser before running the tests
  def setUp(self):
    self.rp = RomanParser()

  #############################################################################
  # The following test methods use a list comprehension to create a list of   #
  # the return values for each of the input strings. Then in the assert call  #
  # the results get compared to an array of all True values of the same size. #
  #############################################################################

  # Test all the input lists that should match

  def test_pos_ones(self):
    resultlist = [ self.rp.isValidRomanNumeral(test)
                   for test in self.posOnesList ]
    self.assertListEqual( resultlist , [True] * len(self.posOnesList),
                          "did not match all ones" )

  def test_pos_tens(self):
    resultlist = [ self.rp.isValidRomanNumeral(test)
                   for test in self.posTensList ]
    self.assertListEqual( resultlist , [True] * len(self.posTensList),
                          "did not match all tens" )

  def test_pos_hundreds(self):
    resultlist = [ self.rp.isValidRomanNumeral(test)
                   for test in self.posHundredsList ]
    self.assertListEqual( resultlist , [True] * len(self.posHundredsList),
                          "did not match all hundreds" )

  def test_pos_thousands(self):
    resultlist = [ self.rp.isValidRomanNumeral(test)
                   for test in self.posThousandsList ]
    self.assertListEqual( resultlist , [True] * len(self.posThousandsList),
                          "did not match all thousands" )

  def test_pos_random(self):
    resultlist = [ self.rp.isValidRomanNumeral(test)
                   for test in self.posRandomList ]
    self.assertListEqual( resultlist , [True] * len(self.posRandomList),
                          "did not match all random numerals" )

  def test_pos_whitespace(self):
    resultlist = [ self.rp.isValidRomanNumeral(test)
                   for test in self.posWhitespaceList ]
    self.assertListEqual( resultlist , [True] * len(self.posWhitespaceList),
                          "did not trim whitespace correctly" )

  def test_pos_capitals(self):
    resultlist = [ self.rp.isValidRomanNumeral(test)
                   for test in self.posCapitalsList ]
    self.assertListEqual( resultlist , [True] * len(self.posCapitalsList),
                          "did not match against capitals correctly" )

  def test_pos_mixedcapitals(self):
    resultlist = [ self.rp.isValidRomanNumeral(test)
                   for test in self.posMixedCapitalsList ]
    self.assertListEqual( resultlist , [True] * len(self.posMixedCapitalsList),
                          "did not match against mixed capitals correctly" )

  # Test all the input lists that should not match

  def test_neg_alpha(self):
    resultlist = [ self.rp.isValidRomanNumeral(test)
                   for test in self.negAlphaList ]
    self.assertListEqual( resultlist , [False] * len(self.negAlphaList),
                          "some non-matches actually matched" )

  def test_neg_empty(self):
    resultlist = [ self.rp.isValidRomanNumeral(test)
                   for test in self.negEmptyList ]
    self.assertListEqual( resultlist , [False] * len(self.negEmptyList),
                          "some non-matches actually matched" )

  def test_neg_random(self):
    resultlist = [ self.rp.isValidRomanNumeral(test)
                   for test in self.negRandomList ]
    self.assertListEqual( resultlist , [False] * len(self.negRandomList),
                          "some non-matches actually matched" )

  def test_neg_whitespace(self):
    resultlist = [ self.rp.isValidRomanNumeral(test)
                   for test in self.negWhitespaceList ]
    self.assertListEqual( resultlist , [False] * len(self.negWhitespaceList),
                          "trimmed the whitespace correctly, whoops" )

class RomanConvertTestCase(unittest.TestCase):
  posOnesList = [ 'i', 'ii', 'iii', 'iv', 'v', 'vi', 'vii', 'viii', 'ix' ]
  posOnesValues = [ 1, 2, 3, 4, 5, 6, 7, 8, 9 ]
  posTensList = [ 'x', 'xx', 'xxx', 'xl', 'l', 'lx', 'lxx', 'lxxx', 'xc' ]
  posTensValues = [ 10, 20, 30, 40, 50, 60, 70, 80, 90 ]
  posHundredsList = [ 'c', 'cc', 'ccc', 'cd', 'd', 'dc', 'dcc', 'dccc', 'cm' ]
  posHundredsValues = [ 100, 200, 300, 400, 500, 600, 700, 800, 900 ]
  posThousandsList = [ 'm', 'mm', 'mmm' ]
  posThousandsValues = [ 1000, 2000, 3000 ]
  posRandomList = [ 'xiv', 'cxl', 'mmmdccclxxxviii', 'mmmcmxcix' ]
  posRandomValues = [ 14, 140, 3888, 3999 ]
  posWhitespaceList = [ ' mm', 'mm ', ' mm ' ]
  posWhitespaceValues = [ 2000, 2000, 2000 ]
  posCapitalsList = [ 'I', 'V', 'X', 'L', 'C', 'D', 'M' ]
  posCapitalsValues = [ 1, 5, 10, 50, 100, 500, 1000 ]
  posMixedCapitalsList = [ 'iI', 'Xl', 'DcC', 'mMm', 'MmmDCcCLxXxVIii' ]
  posMixedCapitalsValues = [ 2, 40, 700, 3000, 3888 ]

  negAlphaList = [ 'a', 'b', 'e', 'f', 'g', 'h', 'j', 'k', 'n', 'o', 'p', 'q',
                   'r', 's', 't', 'u', 'w', 'y', 'z']
  negEmptyList = [ '', ' ', None ]
  negRandomList = [ 'ixv', 'xcl', 'mmmm', 'cccc', 'dd', 'vv', 'll']
  negWhitespaceList = [ ' m m', 'm m ', ' m m ' ]

  def setUp(self):
    self.rp = RomanParser()

  #############################################################################
  # The following test methods use a list comprehension to create a list of   #
  # the return values for each of the input strings. Then in the assert call  #
  # the results get compared to an array of values of the same size.          #
  #############################################################################

  def test_convert_pos_ones(self):
    resultlist = [ self.rp.convertRoman2Decimal(test)
                   for test in self.posOnesList ]
    self.assertListEqual( resultlist , self.posOnesValues,
                          "Did not parse the Roman ones correctly." )

  def test_convert_pos_tens(self):
    resultlist = [ self.rp.convertRoman2Decimal(test)
                   for test in self.posTensList ]
    self.assertListEqual( resultlist , self.posTensValues,
                          "Did not parse the Roman tens correctly." )

  def test_convert_pos_hundreds(self):
    resultlist = [ self.rp.convertRoman2Decimal(test)
                   for test in self.posHundredsList ]
    self.assertListEqual( resultlist , self.posHundredsValues,
                          "Did not parse the Roman hundreds correctly." )

  def test_convert_pos_thousands(self):
    resultlist = [ self.rp.convertRoman2Decimal(test)
                   for test in self.posThousandsList ]
    self.assertListEqual( resultlist , self.posThousandsValues,
                          "Did not parse the Roman thousands correctly." )

  def test_convert_pos_random(self):
    resultlist = [ self.rp.convertRoman2Decimal(test)
                   for test in self.posRandomList ]
    self.assertListEqual( resultlist , self.posRandomValues,
                          "Did not parse the Roman randoms correctly." )

  def test_convert_pos_whitespace(self):
    resultlist = [ self.rp.convertRoman2Decimal(test)
                   for test in self.posWhitespaceList ]
    self.assertListEqual( resultlist , self.posWhitespaceValues,
                          "Did not parse the Roman whitespaces correctly." )

  def test_convert_pos_capitals(self):
    resultlist = [ self.rp.convertRoman2Decimal(test)
                   for test in self.posCapitalsList ]
    self.assertListEqual( resultlist , self.posCapitalsValues,
                          "did not parse the capitals correctly" )

  def test_convert_pos_mixedcapitals(self):
    resultlist = [ self.rp.convertRoman2Decimal(test)
                   for test in self.posMixedCapitalsList ]
    self.assertListEqual( resultlist , self.posMixedCapitalsValues,
                          "did not parse the mixed capitals correctly" )

  # tests that should fail to parse, values returned are -1 
  def test_convert_neg_alpha(self):
    resultlist = [ self.rp.convertRoman2Decimal(test)
                   for test in self.negAlphaList ]
    self.assertListEqual( resultlist , [-1] * len(self.negAlphaList),
                          "some non-matches actually converted" )

  def test_convert_neg_empty(self):
    resultlist = [ self.rp.convertRoman2Decimal(test)
                   for test in self.negEmptyList ]
    self.assertListEqual( resultlist , [-1] * len(self.negEmptyList),
                          "some non-matches actually converted" )

  def test_convert_neg_random(self):
    resultlist = [ self.rp.convertRoman2Decimal(test)
                   for test in self.negRandomList ]
    self.assertListEqual( resultlist , [-1] * len(self.negRandomList),
                          "some non-matches actually converted" )

  def test_convert_neg_whitespace(self):
    resultlist = [ self.rp.convertRoman2Decimal(test)
                   for test in self.negWhitespaceList ]
    self.assertListEqual( resultlist , [-1] * len(self.negWhitespaceList),
                          "converted the whitespace correctly, whoops" )


if __name__ == "__main__":

  suite1 = unittest.TestLoader().loadTestsFromTestCase(RomanTestCase)
  suite2 = unittest.TestLoader().loadTestsFromTestCase(RomanConvertTestCase)
  alltests = unittest.TestSuite([suite1, suite2])
  unittest.TextTestRunner(verbosity=2).run(alltests)
