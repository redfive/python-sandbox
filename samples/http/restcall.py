#!/usr/bin/env python

import json
import urllib
import urllib2

# Take one, using core Python interfaces
#resp = urllib2.urlopen("http://localhost:8000/api/blogpost/?format=json")
#data = json.load(resp)
#print data

# Take two, using the python-rest-client from google code

from restful_lib import Connection

conn = Connection ("http://localhost:8000/api")
#resp = conn.request_get( "/blogpost", args={ 'format' : 'json' }, headers={ 'accept' : 'text/json' } )
resp = conn.request_get( "/blogpost", headers={ 'accept' : 'text/json' } )

for r in resp.keys():
  print "%s ===> %s" % (r, resp[r])
