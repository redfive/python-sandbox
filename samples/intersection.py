#!/usr/bin/env python

"""
    An interview question to return the character that had the
    greatest intersection between two strings.
    i.e. 'aaabbcc' and 'babaacddd' would have character intersections
    of a=3 b=2 c=1 d=0 and would return 'a'
"""
def string_intersection(string_a, string_b):
    """
    Find the character with the greatest occurence in both strings
    """
    char_counts_a = {}
    char_counts_b = {}

    for char in string_a:
        if char_counts_a.has_key(char):
            char_counts_a[char] += 1
        else:
            char_counts_a[char] = 1

    for char in string_b:
        if char_counts_b.has_key(char):
            char_counts_b[char] += 1
        else:
            char_counts_b[char] = 1

    # at this point have counts for both strings

    max_char = ''
    max_count = 0
    count_a_keys = char_counts_a.keys()
    for key in count_a_keys:
        if char_counts_b.has_key(key):
            count_a = char_counts_a[key]
            count_b = char_counts_b[key]
            intersection_count = 0
            if count_a < count_b:
                # go with count_a
                intersection_count = count_a
            else:
                # go with count_b
                intersection_count = count_b

            if intersection_count > max_count:
                max_char = key
                max_count = intersection_count

    return (max_char, max_count)

print "{}".format(string_intersection('aaabbbcccddd', 'aabbccccdd'))
print "{}".format(string_intersection('aabcbcbd', 'abbbccccceeeeeeeeeeeeeeee'))
print "{}".format(string_intersection('abcabcabcabcabcabcabcabdbacdbacacdacabd',
                                      'ddddddddddddddbbbbbbbbbbbccccccccaaaaaa'))
