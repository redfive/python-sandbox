#!/usr/bin/env python

from optparse import OptionParser
import re
import sys
import unittest

class RomanParser():
  '''
   Class to validate a roman numeral and to convert from roman to decimal.

   Reasonably accpeted rules used to determine valid Roman numerals are:
     - I, X, C, M can be repeated up to 3 times in succession
     - D, L, V can never repeat 
     - I can only subtract from V, X
     - X can only subtract from L, C
     - C can only subtract from D, M
     - M, D, L, V can never subtract
     - Only one subtraction can happen at a time
   This points to an implementation of a state machine which would work,
   but would be cumbersome to code and more difficult to extend than if
   one used regular expressions.

   Roman numerals are limited between 1 and 3999 and have distinct values
   for the thousands, hundreds, tens and ones places. Those possible values
   are what built up the regex used within.
     000s : M, MM, MMM
     00s : C, CC, CCC, CD, D, DC, DCC, DCCC, CM
     0s : X, XX, XXX, XL, L, LX, LXX, LXXX, XC
     1s : I, II, III, IV, V, VI, VII, VIII, IX

   It would be trival to add, for instance, a ten thousands place and have
   K = 10000 and R = 5000 and include both an extension to the regex and
   the ability to convert to the larger number set.
  '''

  # values to use for converting from Roman to Decimal
  onesValues = { 'i': 1, 'ii': 2, 'iii': 3, 'iv': 4, 'v': 5,
                 'vi': 6, 'vii': 7, 'viii': 8, 'ix': 9 }
  tensValues = { 'x': 10, 'xx': 20, 'xxx': 30, 'xl': 40, 'l': 50,
                 'lx': 60, 'lxx': 70, 'lxxx': 80, 'xc': 90 }
  hundredsValues = { 'c': 100, 'cc': 200, 'ccc':300, 'cd': 400, 'd':500,
                     'dc': 600, 'dcc': 700, 'dccc': 800, 'cm': 900 }
  thousandsValues = { 'm': 1000, 'mm': 2000, 'mmm': 3000 }

  # create the regex pattern to validate the roman numerals. Use named
  # patterns so that we can capture and reuse the matches to build up
  # the value in decimal form.
  pattern = '''
            ^                                  # Start at the beginning
            (?P<thousands>M{0,3})              # thousands digit, 1000 to 3000
            (?P<hundreds>C{0,3}|CD|DC{0,3}|CM) # hundreds digit, 100 to 900
            (?P<tens>X{0,3}|XL|LX{0,3}|XC)     # tens digit, 10 to 90
            (?P<ones>I{0,3}|IV|VI{0,3}|IX)     # ones digit, 1 to 9
            $                                  # match all the way to the end
            '''

  # pre compile the regex for performance
  regex = re.compile(pattern, re.VERBOSE | re.IGNORECASE)

  def isValidRomanNumeral( self, aString ):
    '''
      Checks the input string for empty and null values and
      trims the whitespace from the ends.
      @returns True if the input string is a valid Roman numeral
      @returns False if the input string is empty, None or an
               invalid Roman numeral.
    '''

    # guard against nothing
    if aString == None:
      return False

    # strip whitespace
    testString = aString.strip()

    # check for an empty string
    if testString == "":
      return False

    # if valid search with return a Match object
    return bool(self.regex.search(testString))

  def convertRoman2Decimal( self, aString ): 
    '''
     Checks the input string to see if it's a valid Roman numeral
     before parsing it and converting to decimal. Uses the same
     regex as isValidRomanNumeral.
     @returns -1 if the input string is not a valid Roman numeral
     @returns the value of the Roman numeral if it is valid: 1-3999
    '''
    if not self.isValidRomanNumeral(aString):
      return -1

    # strip whitespace
    testString = aString.strip()

    match = self.regex.search(testString)
    value = 0

    # go through each of the groups and add the value to the running total
    if match.group('ones'):
      value += self.onesValues[match.group('ones').lower()]
    if match.group('tens'):
      value += self.tensValues[match.group('tens').lower()]
    if match.group('hundreds'):
      value += self.hundredsValues[match.group('hundreds').lower()]
    if match.group('thousands'):
      value += self.thousandsValues[match.group('thousands').lower()]

    return value

  # End RomanParser
 
if __name__ == "__main__":

  # configure options for running this script from the CLI
  usage = "%prog [ -c/--check NUMERAL | -p/--parse NUMERAL ]"
  parser = OptionParser(usage=usage)
  parser.add_option( '-c', '--check', dest='check', action="store", default=False,
                     help="check the roman numeral passed in and dump results to stdout." )
  parser.add_option( '-p', '--parse', dest='parse', action="store", default=False,
                     help="parse the roman numeral passed in and dump the decimal form to stdout." )

  (options, args) = parser.parse_args()

  if options.check and options.parse :
    parser.error("only one of the options can be used at once")

  rp = RomanParser()

  if options.parse:
    # convert the passed in Roman numeral and dump decimal to stdout
    decimal = rp.convertRoman2Decimal(options.parse)
    print "Your numeral is %s" % (decimal)
  elif options.check:
    # parse the passed in Roman numeral and respond to user on stdout
    if rp.isValidRomanNumeral(options.check):
      print "Nice roman numeral!"
    else:
      print "Roman numerals are hard, let's go shopping"
  else:
    # prompt the user for a Roman numeral, parse it and respond on stdout
    while True:
      try:
        numeral = raw_input("Enter a roman numeral: ")
      except:
        # catch a ctrl-c or ctrl-d
        print "Goodbye."
        break; 

      # exit if user types q
      if numeral == 'q':
        print "Goodbye."
        break;

      if rp.isValidRomanNumeral(numeral):
        decimal = rp.convertRoman2Decimal(numeral)
        print "Nice roman numeral:%d !" % (decimal)
      else:
        print "Roman numerals are hard, let's go shopping"
      

