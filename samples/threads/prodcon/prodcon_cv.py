#!/usr/bin/env python

import threading
import random
from time import sleep

""" A basic Producer/Consumer threading issue handled with
    Locks and Condition variables.
"""

class Producer(threading.Thread):

  def __init__(self, aBuffer):
    threading.Thread.__init__(self)
    self.buffer = aBuffer

  def run(self):
    # add things to the int buffer
    while True:
      num = random.randint(0,100)
      self.buffer.add(num)
      sleep(1)
      print 'produced',  num
    

class Consumer(threading.Thread):

  def __init__(self, aBuffer):
    threading.Thread.__init__(self)
    self.buffer = aBuffer

  def run(self):
    while True:
      num = self.buffer.remove()
      sleep(2)
      print 'consumed', num

class IntBuffer(object):
  numList = []
  MAX_LENGTH = 8

  def __init__(self):
    self.lock = threading.Lock()
    self.cv = threading.Condition(self.lock)

  def add(self, aNumber):
    self.cv.acquire()
    if len(self.numList) == self.MAX_LENGTH:
      self.cv.wait()
    self.numList.append(aNumber)
    self.cv.notify()
    self.cv.release()

  def remove(self):
    self.cv.acquire()
    if len(self.numList) == 0:
      self.cv.wait()
    retval = self.numList.pop()
    self.cv.notify()
    self.cv.release()
    return retval

if __name__ == "__main__":
  buff = IntBuffer()
  prod = Producer(buff)
  cons = Consumer(buff)
  prod.start()
  cons.start()
