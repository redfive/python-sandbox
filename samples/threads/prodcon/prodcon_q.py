#!/usr/bin/env python

import threading
import Queue
import random
from time import sleep

""" A basic Producer/Consumer threading issue handled with
    a Queue structure.
"""

class Producer(threading.Thread):

  def __init__(self, aQueue):
    threading.Thread.__init__(self)
    self.queue = aQueue

  def run(self):
    # add things to the int queue
    while True:
      num = random.randint(0,100)
      self.queue.put(num)
      sleep(1)
      print 'produced',  num
    

class Consumer(threading.Thread):

  def __init__(self, aQueue):
    threading.Thread.__init__(self)
    self.queue = aQueue

  def run(self):
    while True:
      num = self.queue.get()
      sleep(2)
      print 'consumed', num

if __name__ == "__main__":
  queue = Queue.Queue()
  prod = Producer(queue)
  cons = Consumer(queue)
  prod.start()
  cons.start()
