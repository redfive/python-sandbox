#!/usr/bin/env python

import threading
from time import ctime

loops = (8, 4, 2)
"""
   A generic thread class for calling a function on a background thread.
   Use like (see also mtsleepE.py):

     def loop(nloop, nsec):
       sleep(nsec)
     t = MyThread( loop, (1, 5), loop.__name__)
     t.start()
"""
class MyThread(threading.Thread):

  def __init__(self, func, args, name=''):
    threading.Thread.__init__(self)
    self.func = func
    self.args = args
    self.name = name

  def run(self):
    print 'Starting', self.name, 'at:', ctime()
    self.result = self.func(*self.args)
    print self.name, 'Finished at:', ctime()

  def getResult(self):
    return self.result

