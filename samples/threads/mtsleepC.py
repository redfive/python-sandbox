#!/usr/bin/env python

import threading
from time import sleep, ctime

loops = [8, 4, 2]

def loop(nloop, nsec):
  print 'starting loop', nloop, 'at:', ctime(), 'sleeping for', nsec, 'seconds'
  sleep(nsec)
  print 'loop', nloop, 'done at:', ctime()

""" use threading.Thread in conjunction with a simple function. The
    args will get passed to it when the thread is started.
"""
def main():
  print 'starting at:', ctime()
  threads = []
  nloops = range(len(loops))

  for i in nloops:
    t = threading.Thread(target=loop, args=(i, loops[i]))
    threads.append(t)

  for i in nloops:
    threads[i].start()

  # this blocks on the each call to join until the thread finishes.
  for i in nloops:
    print 'joining with thread', i, 'at:', ctime()
    threads[i].join()
  #sleep(10)

  print 'All done at:', ctime()

if __name__ == "__main__":
  main()
  
