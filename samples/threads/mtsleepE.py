#!/usr/bin/env python

import threading
from time import sleep, ctime

from mythread import MyThread

loops = (8, 4, 2)

def loop(nloop, nsec):
  sleep(nsec)

""" Use a class that subclasses threading.Thread. Now the
    implementation of the subclass is the part that is designed
    to call a passed in function. It *could* do anything it wants
    in its run method.
"""
def main():
  print 'starting at:', ctime()
  threads = []
  nloops = range(len(loops))

  for i in nloops:
    t = MyThread( loop, (i, loops[i]), loop.__name__)
    threads.append(t)

  for i in nloops:
    threads[i].start()

  for i in nloops:
    print 'joining with thread', i, 'at:', ctime()
    threads[i].join()

  print 'All done at:', ctime()

if __name__ == "__main__":
  main()
  
