
class Node:
  adjNodes []

# node copyNode ( aNode, aVisitedNodes )
def copyNode (aNode, aVisitedNodes):
  aVisitedNodes.append(aNode)

  # create a new node to hand back
  newNode = Node()
  
  # for each of the adjacent nodes, copy each node and insert into the
  # new node's adjNodes list
  for adjnode in aNode.adjNodes:
    if adjnode in aVisitedNodes:
      # we've seen the node, don't copy, add to adjNodes
      newNode.adjNodes.append(findCopiedAdjNode(adjnode))
    else:
      newNode.adjNodes.append(copyNode(adjnode, aVisitedNodes))

  return newNode


# another try

class Node():
  unique_id = ""
  adj_nodes = []

def copyNode (aNode, aCopiedNodes):

  # copy the node
  newNode = Node(aNode.unique_id)

  # add it to the copied hash so we don't loop
  aCopiedNodes[newNode.unique_id] = newNode

  for adjNode in aNode.adj_nodes:
    if adjNode.unique_id in aCopiedNodes:
      # we've been to this node already, don't recurse, just to the newNode
      newNode.adj_nodes.append(aCopideNodes[adjNode.unique_id])
    else:
      newNode.adj_nodes.append(copyNode(adjNode))

  return newNode




# end another try







findSubstring ( "cardealer", "deal" );
returns 3

dealer    deal
"" ""
dealer ""
"" deal

cardealer   lerk

cardealer  chair
aaaa     bbbb
aaabb   aabb
aaaaaaaaaaaaa    aaaaaaaaaaaaaaaaaaaaaaaaaaaa

#another try

def findSubString(sourceString, subString):
  if not sourceString or
     not subString:
    return -1

  sourceLen = len(sourceString)
  subLen = len(subString)

  if subLen > sourceLen:
    return -1

  cur_start = 0
  src_index = 0
  sub_index = 0

  # loop over the source string, looking for the sub string
  while cur_start < sourceLen:
    src_index = cur_start
    while src_index < sourceLen and sub_index < subLen:
      if sourceString[src_index] == subString[sub_index]:
        # we have a matching character
        src_index += 1
        sub_index += 1
      else:
        # no match break out
        break
    if sub_index == subLen and src_index <= sourceLen:
      # we found the entire subString in sourceString
      break
    # we got to the end of the indexes so we have to start over
    cur_start += 1
    sub_index = 0
    # src_index gets set at the top of loop

  if cur_start == sourceLen:
    cur_start = -1

  return cur_start
    
    

def findSubstring ( aString, aSubString ):
  if aString == "":
    return -1

  if aSubString = "":
    return -1

  stringLen = len(aString)
  substringLen = len(aSubString)
  # iterate over the search string
  index = 0
  subindex = 0
  startindex = -1
  while index < stringLen:
    if aString[index] == aSubString[subindex]:
      # track the starting position if not already in a match
      if startindex == -1:
        startindex = index

      # have matched chars
      if subindex == substringLen-1:
        # reached the end of the substring, we have a match
        break
      subindex += 1
      index += 1
    else:
      index = startindex + 1
      subindex = 0
      startindex = -1

  # guard against a partial match at the end of aString
  if subindex != subStringLen-1:
    startindex = -1

  return startindex    
      
