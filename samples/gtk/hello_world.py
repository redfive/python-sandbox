#!/usr/bin/env python

import pygtk
pygtk.require('2.0')
import gtk

class HelloWorld:

  def hello(self, widget, data=None):
    print "Hello World"

  def delete_event(self, widget, event, data=None):
    print "delete event occured"
    # destroy the window
    return False

  def destroy(self, widget, data=None):
    print "destroy signal occured"
    gtk.main_quit()

  def __init__(self):
    self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)

    self.window.set_border_width(10)

    # hook up delete event handler (for window close operations)
    self.window.connect("delete_event", self.delete_event)

    # hook up destroy signal handler (called when gtk_widget_destroy is called or False is
    # returned by the delete_event handler)
    self.window.connect("destroy", self.destroy)

    self.button = gtk.Button("Word")

    self.button.connect("clicked", self.hello, None)
    self.button.connect_object("clicked", gtk.Widget.destroy, self.window)

    self.window.add(self.button)
    self.button.show()
    self.window.show()

  def main(self):
    gtk.main()


if __name__ == "__main__":
  hello = HelloWorld()
  hello.main()

