#!/usr/bin/env python

import sys
import time

HOME = 1
DIR_STOPPED = "Stopped"
DIR_UP = "Up" 
DIR_DOWN = "Down" 

class Elevator():
  cur_floor = HOME
  cur_direction = DIR_STOPPED
  dest_floor = HOME

  up_list = []
  down_list = []

  def getInput(self):
    if self.cur_direction == DIR_STOPPED:
      input = raw_input("[%s - %s] Enter call or go (exit to quit):" % (self.cur_floor, self.cur_direction ))
    else:
      input = raw_input("[%s - %s] Enter next call (exit to quit):" % (self.cur_floor, self.cur_direction))
    return input

  def enqueue(self, input):
    parsedInput = input.split(";")
    for action in parsedInput:
      if action == "":
        continue
      action = action.strip()
      parsedAction = action.split(' ')
      command = parsedAction[0]
      floor = int(parsedAction[1])

      if command == "call":
        direct = parsedAction[2]
        if self.cur_floor > floor:
          # input floor is below us
          #if self.cur_direction == DIR_STOPPED:
          #  # we were stopped, so set our direction
          #  self.cur_direction = DIR_DOWN
          self.down_list.append(floor)
          self.down_list.sort()
          print "down: ", self.down_list

        if self.cur_floor < floor:
          # input floor is above us
          #if self.cur_direction == DIR_STOPPED:
          #  # we were stopped, so set our direction
          #  self. cur_direction = DIR_UP
          self.up_list.append(floor)
          self.up_list.sort()
          print "up: ", self.up_list

        if self.cur_floor == floor:
          print "Same floor"
          
      elif command == "go":
        direct = ''

      # reset
      command = floor = direct = ''

  def move(self):
    if self.cur_direction == DIR_STOPPED:
      upLen = len(self.up_list)
      downLen = len(self.down_list)
      import ipdb
      ipdb.set_trace()
      if upLen == 0 and downLen == 0:
        return

      if upLen > downLen:
        self.dest_floor = self.up_list.pop()
        self.cur_direction = DIR_UP
      else:
        self.dest_floor = self.down_list.pop()
        self.cur_direction = DIR_DOWN

    if self.cur_direction == DIR_UP:
      # move up a 
      print "going up: ", self.dest_floor
      self.cur_floor += 1
      if self.cur_floor == self.dest_floor:
        if len(self.up_list) == 0:
          self.cur_direction = DIR_STOPPED

    if self.cur_direction == DIR_DOWN:
      print "going down: ", self.dest_floor
      self.cur_floor -= 1
      if self.cur_floor == self.dest_floor:
        if len(self.down_list) == 0:
          self.cur_direction = DIR_STOPPED


  def start(self):
    while True:
      input = self.getInput()
      if input == "":
        self.move()
      else:
        self.enqueue(input)
      

if __name__ == "__main__":

    ele = Elevator()
    ele.start()
    exit()

      
