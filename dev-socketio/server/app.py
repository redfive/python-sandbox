import os

from flask import Flask, jsonify, render_template
from flask_socketio import emit, SocketIO

app = Flask(__name__)
app.config['SECRET_KEY'] = 'totally_secret'
socketio = SocketIO(app)

@socketio.on('my event', namespace='/test')
def test_message(message):
    emit('my response', {'data': message['data']})
    print('Client sent message: ' + message['data'])

@socketio.on('my broadcast event', namespace='/test')
def test_message(message):
    emit('my response', {'data': message['data']}, broadcast=True)
    print('Client broadcasted message: ' + message['data'])

@socketio.on('connect', namespace='/test')
def test_connect():
    emit('my response', {'data': 'Connected'})
    print('Client Connected')

@socketio.on('disconnect', namespace='/test')
def test_disconnect():
    print('Client disconnected')

if __name__ == "__main__":
    # use the socketio way of running
    # TODO use ipython to trace this
    socketio.run(app)
