import functools
import time

# Define some ancillary functions to get called to log duration

def burger_adder(duration):
    print "In the burger_adder. duration is: %f" % (duration)

def context_adder(duration):
    print "In the context_adder. duration is: %f" % (duration)

# This is a decorator factory that will spit out customized decorators
def calltimer_decorator_maker(adder, statsd_prefix=None, method=False):
    print "In the maker"

    # This is the actual decorator that gets returned.
    def calltimer_decorator(func):
        print "  In the decorator"

        # tricky local decorator to handle methods & functions
        #def calltiming(f):
        #    def _timing_wrapper(*args, **kwargs):
        #        try:
        #            start = time.time()
        #            # This is the call to the original function
        #            return f(*args, **kwargs)
        #        finally:
        #            elapsed = (time.time() - start) * 1000
        #            adder(elapsed)
        #    return _timing_wrapper

        # This is the wrapper that the decorator will hand back when it wraps
        # a function.
        #if method:
        #    print "making a method wrapper"
        #    @functools.wraps(func)
        #    @calltiming
        #    def _wrapper(cls_or_self, *args, **kwargs):
        #        return func(cls_or_self, *args, **kwargs)
        #else:
        #    print "making a function wrapper"
        #    @functools.wraps(func)
        #    @calltiming
        #    def _wrapper(*args, **kwargs):
        #        return func(*args, **kwargs)

        @functools.wraps(func)
        def _wrapper(*args, **kwargs):
            """ When the initial function that was wrapped gets called this code
                block will execute.
            """

            print "    In the wrapper, about to return the value from the func"
            try:
                start = time.time()
                # This is the call to the original function
                return func(*args, **kwargs)
            finally:
                elapsed = (time.time() - start) 
                adder(elapsed)

        print "  returning the wrapper"
        return _wrapper

    print "returning the decorator"
    return calltimer_decorator

#print "\n"
#print "Creating a decorator"
#context_timer_decorator = calltimer_decorator_maker(context_adder)
#
# Removing to test the classmethod impl
#print "\n"
#print "Applying the decorator at definition time using '@'"
#@context_timer_decorator
#def do_context(*args, **kwargs):
#    print "In do_context"
#
##do_context = context_timer_decorator(do_context)
#print "\n"
#print "Calling the decorated function"
#do_context()

print "\n"
print "Creating another decorator"
burger_timer_decorator = calltimer_decorator_maker(burger_adder)

class my_class(object):
    print "\n"
    print "Applying the decorator at definition time using '@'"
    @classmethod
    @burger_timer_decorator
    def do_burger(cls, *args, **kwargs):
        print "do_burger: in do_burger"
        print "cls type: %s, %s" % (type(cls), cls)

    @classmethod
    def do_test(*args, **kwargs):
        print "do_test: cls type: %s, %s" % (type(args[0]), args[0])
        arglist = [repr(arg) for arg in args]
        kwarglist = ['%r=%r' % i for i in kwargs.iteritems()]
        print 'args:(%s)' % (', '.join(arglist))
        print 'kwargs:(%s)' % (', '.join(kwarglist))


    #print "\n"
    #print "Applying the decorator"
    #do_burger = burger_timer_decorator(do_burger)

print "\n"
print "Calling the decorated classmethod"
mc = my_class()
mc.do_burger()

my_class.do_burger()
mc.do_test("test1", test2="test2")

