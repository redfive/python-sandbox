# Include the Dropbox SDK libraries
from dropbox import client, rest, session

# Get your app key and secret from the Dropbox developer website
APP_KEY = '**REDACTED**'
APP_SECRET = '**REDACTED**'

# ACCESS_TYPE should be 'dropbox' or 'app_folder' as configured for your app
ACCESS_TYPE = 'dropbox'

sess = session.DropboxSession(APP_KEY, APP_SECRET, ACCESS_TYPE)

request_token = sess.obtain_request_token()

# Make the user sign in and authorize this token
url = sess.build_authorize_url(request_token)
print "url:", url
print "Please authorize in the browser. After you're done, press enter."
raw_input()

# This will fail if the user didn't visit the above URL and hit 'Allow'
# stash the access token for later use. It only needs to be created if
# a) user has not ever given access
# b) user has re-installed the app
# c) user has revoked access via the Dropbox website
access_token = sess.obtain_access_token(request_token)
print "access_token:", access_token

client = client.DropboxClient(sess)
print "linked account:", client.account_info()
