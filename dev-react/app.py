import os
import docker

from flask import Flask, jsonify, render_template

app = Flask(__name__)

@app.route("/")
def index():
    return render_template('index.html')

@app.route("/getinsult")
def get_insult():
    # Get the text for the insult, from another server
    retval =  jsonify({ 'insult': 'scruffy nerfherder' })

    # Get the image
    # TODO

    # pass the text and image to a library to create the final picture
    # TODO

    # stream the final image back to the end user.
    # TODO

    print (retval)
    return retval

@app.route("/containers")
def get_containers():
    cli = docker.from_env()
    conts = cli.containers.list()
    containers = []
    for c in conts:
        cont = { 'id' : c.short_id,
                 'image' : c.attrs['Config']['Image'] }
        containers.append(cont)
    retval = jsonify({ 'containers': containers })
    print(retval)
    return retval

if __name__ == "__main__":
    # this works for running via `python` but not `flask run`
    app.run(port=int(os.environ.get("PORT", 5000)), debug=True )
