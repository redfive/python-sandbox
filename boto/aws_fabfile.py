import logging

FORMAT = 'REDFIVE*** %(message)s'
logging.basicConfig(format=FORMAT, level=logging.INFO)
logger = logging.getLogger(__file__)

'''
I want to be able to call this like so:

  # start 3 new aws thor nodes, add them to the network start sending them traffic
  fab -f aws_fabfile conf:thor,prod start:3 enable

  # find the new and old nodes
  fab -f aws_fabfile conf:thor,prod list_nodes:

  # unplug old nodes from the LB
  fab -f aws_fabfile conf:thor,prod disable:old

  # 
'''

def launch_instance(ami='default'):
    logger.info('launch_instance {0}'.format(ami))
    # TODO: 
    ii = self.config.get('aws.image_id')
    it = instance_type=self.config.get('aws.instance_type')
    kn = key_name=self.config.get('aws.key_name')
    sg = security_groups=self.config.get('aws.security_groups').split(' ')
    si = subnet_id=self.config.get('aws.subnet_id')
    logger.info('Paws.start_instances {0}, {1}, {2}, {3}, {4}'.format(ii, it, kn, sg, si))
    import ipdb; ipdb.set_trace()
    reservation = self.conn.run_instances(
        ii,
        instance_type=it,
        key_name=kn,
        security_group_ids=sg,
        subnet_id=si
        )

def configure_chef():
  logger.info('configure_chef')
