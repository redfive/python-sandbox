
config = {
  'com': {
    'jgaunt': {
      'aws': {
        'region': 'us-west-2',
        'image_id': 'ami-1d',
        'instance_type': 'm3.medium',
        'key_name': 'j@r.o',
        'security_groups': ['sg-03', 'sg-a1', 'sg-0'],
        'subnet_id': 'subnet-f3'
      }
    }
  }
}
