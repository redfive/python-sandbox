#!/usr/bin/env python

import argparse
import boto.ec2 as ec2
import logging
import sys

from parser import (ConfigService, ArgParseFactory)

FORMAT = 'REDFIVE*** %(message)s'
logging.basicConfig(format=FORMAT, level=logging.INFO)
logger = logging.getLogger(__file__)

#def help():
#  print 'instance mgmt with paws: paws.py instance [list | launch | connect(?)]'
#  print ' launch would take additional configuration, as would connect'
#  print 'configure paws: paws.py config <filename> [<filename>...]'
#  print ' this will set up the configuration for the tool and any subsequent commands'

class Paws:
  """
  Python AWS tool.
  """
  config = None
  conn = None

  def __init__(self, config_service):
    # scope all the config requests to this tool -- consider allowing this to
    # be passed in. A project using the paws tool may want it's own settings
    config_service.set_scope('com.jgaunt.paws')
    self.config = config_service

  def connect(self):
    if not self.config:
      # rasie an exception
      return

    # TODO: try/catch here
    # connect to a region using our configuration
    self.conn = ec2.connect_to_region(self.config.get('aws.region'))
                                      #aws_access_key_id=self.config.get('aws.access_key_id'),
                                      #aws_secret_access_key=self.config.get('aws.secret_access_key'))

    if not self.conn:
      logger.warn('Did NOT connect to ec2')
      # raise an exception

  def handle_instance_subcommand(self, args):
    logger.info('Paws.handle_instance_subcommand: {}'.format(args))
    if not self.conn:
      self.connect()

    command = args['instance_subcommand']
    logger.info('Paws.handle_instance_subcommand: {}'.format(command))
    if command == 'list':
      self.list_instances()
    elif command == 'start':
      self.start_instances()

  def list_instances(self):
    logger.info('Paws.list_instances')
    instances = self.conn.get_only_instances()
    logger.info('Paws.list_instances: found {}'.format(instances))
    return instances

  def start_instances(self):
    '''
    data=`aws ec2 run-instances \
       --image-id ${image_id} \
       --iam-instance-profile Name=bigdata-readonly \
       --instance-type m3.medium \
       --region us-west-2 \
       --key-name j@r.o \
       --security-group-ids "${sg_sandbox}" "${sg_admin_rdio_dev}" "${sg_infra_vpn_tools}" \
       --subnet-id ${sn_sandbox_us_west_2c} \
       --user-data ${run_list} \
       --output text \
       --query 'Instances[*].[InstanceId, NetworkInterfaces[*].PrivateIpAddress]'`
    '''
    logger.info('Paws.start_instances')
    ii = self.config.get('aws.image_id')
    it = instance_type=self.config.get('aws.instance_type')
    kn = key_name=self.config.get('aws.key_name')
    sg = security_groups=self.config.get('aws.security_groups').split(' ')
    si = subnet_id=self.config.get('aws.subnet_id')
    logger.info('Paws.start_instances {0}, {1}, {2}, {3}, {4}'.format(ii, it, kn, sg, si))
    import ipdb; ipdb.set_trace()
    reservation = self.conn.run_instances(
        ii,
        instance_type=it,
        key_name=kn,
        security_group_ids=sg,
        subnet_id=si
        )
    import ipdb; ipdb.set_trace()



def main():
  parser = ArgParseFactory.build_parser_from_file('./parser_config.py')
  ar = parser.parse_args()

  args = vars(ar)
  logger.info('Parsed arguments: %s' % (args))

  paws = Paws(ConfigService(ConfigService.CONSUL))

  if args.has_key('instance_subcommand'):
    paws.handle_instance_subcommand(args)

if __name__ == "__main__":
  sys.exit(main())
