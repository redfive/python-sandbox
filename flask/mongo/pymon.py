#!/usr/bin/env python

from flask import Flask
from flask.ext.pymongo import PyMongo
 
app = Flask (__name__)

mongo = PyMongo(app)

@app.route('/user/<name>', methods=['GET'])
def show_user(name):
  user = mongo.db.users.find_one_or_404({'name' : name})
  return user

@app.route('/user/<new_name>', methods=['POST'])
def add_user(new_name):
  #name_cursor = mongo.db.users.find_one({name : name}) 
  #if name_cursor.count() == 0:
  user = mongo.db.user.insert({'name' : new_name})
  return str(user)
  

if __name__ == '__main__':
  app.run(debug = True)

