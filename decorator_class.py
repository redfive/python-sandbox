import functools
import time

# Define some ancillary functions to get called to log duration

def burger_adder(duration):
    print "In the burger_adder. duration is: %f" % (duration)

def context_adder(duration):
    print "In the context_adder. duration is: %f" % (duration)

class CallTimerDecorator(object):
    def __init__(self, func):
        print "In CallTimerDecorator.__init__"
        self.func = func
        functools.update_wrapper(self, func)

    def __get__(self, obj, type=None):
          return self.__class__(self.func.__get__(obj, type))

    def __call__(self):
        print "In CallTimerDecorator.__call__"
        return self.func()

print "\n"
print "Decorating do_context_class"
@CallTimerDecorator
def do_context_class():
    print "In do_context_class"

print "\n"
print "Calling do_context_class"
do_context_class()


# This is a decorator factory that will spit out customized decorators
def calltimer_decorator_maker(adder, statsd_prefix=None):
    print "In the maker"

    # This is the actual decorator that gets returned.
    def calltimer_decorator(func):
        print "  In the decorator"

        # This is the wrapper that the decorator will hand back when it wraps
        # a function.
        @functools.wraps(func)
        def wrapped(cls_or_self, *args, **kwargs):
            """ When the initial function that was wrapped gets called this code
                block will execute.
            """

            print "    In the wrapper, about to return the value from the func"
            try:
                start = time.time()
                # This is the call to the original function
                return func(cls_or_self, *args, **kwargs)
            finally:
                elapsed = (time.time() - start) * 1000
                adder(elapsed)

        print "  returning the wrapped"
        return wrapped

    print "returning the decorator"
    return calltimer_decorator

#print "\n"
#print "Creating a decorator"
#context_timer_decorator = calltimer_decorator_maker(context_adder)
#
# Removing to test the classmethod impl
#print "\n"
#print "Applying the decorator at definition time using '@'"
#@context_timer_decorator
#def do_context(*args, **kwargs):
#    print "In do_context"
#
##do_context = context_timer_decorator(do_context)
#print "\n"
#print "Calling the decorated function"
#do_context()

#print "\n"
#print "Creating another decorator"
#burger_timer_decorator = calltimer_decorator_maker(burger_adder)
#
#class my_class(object):
#    print "\n"
#    print "Applying the decorator at definition time using '@'"
#    @classmethod
#    @burger_timer_decorator
#    def do_burger(self, *args, **kwargs):
#        print "in do_burger"
#
#    #print "\n"
#    #print "Applying the decorator"
#    #do_burger = burger_timer_decorator(do_burger)
#
#print "\n"
#print "Calling the decorated classmethod"
#mc = my_class()
#mc.do_burger()
#


