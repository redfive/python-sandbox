
config = {
  'com': {
    'jgaunt': {
      'aws': {
        'aws_access_key_id': '**REDACTED**',
        'aws_secret_access_key': '**REDACTED**',
        'region': 'us-west-1'
      }
    }
  }
}
