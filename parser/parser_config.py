
parser = {
  'version' : 'consul://com.jgaunt.paws.version',
  'name': 'paws',
  'subparsers' : {
    # gets set as the help for the add_subparser call
    'help' : 'Subcommands for working with specific parts of AWS',
    'name': 'paws_subcommand_name',
    'parsers' : [
      {
        'name': 'instance',
        'help' : 'EC2 Instance Operations',
        'subparsers' : {
          'help' : 'Actions',
          'name': 'instance_subcommand_name',
          'parsers' : [
            {
              'name': 'list',
              'help': 'List all instances in the account'
            },
            {
              'name': 'start',
              'help': 'Start the given instance',
              'args' : [
                {
                  'name': 'id',
                  'action' : 'store',
                  'help' : 'AWS instance id to start'
                }
              ]
            }
          ]
        }
      },
      {
        'name': 'ami',
        'help' : 'AMI Operations'
      },
      {
        'name': 's3',
        'help' : 'S3 Operations'
      },
      {
        'name': 'config',
        'help' : 'Configuration Operations'
      }
    ]
  },
  'args': [
    {
      'name': '-q',
      'action' : 'store_true',
      'dest': 'quiet',
      'default': False,
      'help' : 'Quiet the output'
    }
  ]
}
