import consulate
import nose.tools as nt

from parser import ConfigService

class TestConfigService(object):
  version = 1337
  config_prefix = 'com.jgaunt.tests.ConfigService'

  @classmethod
  def setup_class(cls):
    cls.consul = consulate.Consul()
    cls.consul.kv.set('{}.version'.format(cls.config_prefix), cls.version)
    cls.config_service = ConfigService(ConfigService.CONSUL)

  @classmethod
  def teardown_class(cls):
    #cls.consul.kv.delete(cls.config_prefix)
    cls.consul = None

  def test_create(self):
    assert self.config_service
    assert hasattr(self.config_service, 'consul')

  def test_get_key(self):
    nt.assert_equal(self.config_service.get('{}.version'.format(self.config_prefix)), self.version)

  def test_set_key(self):
    self.config_service.set('{}.version'.format(self.config_prefix), 'test value')
    nt.assert_equal(self.config_service.get('{}.version'.format(self.config_prefix)), 'test value')

  def test_set_file(self):
    # TODO: os.path.join to add the root dir to the filename
    self.config_service.set_from_file('./test_config.py')

  def test_scope_set(self):
    self.config_service.set_scope('com.jgaunt.config_service.test')
    self.config_service.set('foo.bar', 'baz')
    self.config_service.unset_scope()
    nt.assert_equal(self.config_service.get('com.jgaunt.config_service.test.foo.bar'), 'baz')

  def test_scope_get(self):
    self.config_service.set('com.jgaunt.config_service.test.foo.bar', 'baz')
    self.config_service.set_scope('com.jgaunt.config_service.test')
    nt.assert_equal(self.config_service.get('foo.bar'), 'baz')

class TestParser(object):
  def test_from_file(self):
    pass

  def test_from_object(self):
    pass
