
config = {
  'com': {
    'jgaunt': {
      'aws': {
        'test': {
          'aws_access_key_id': '**REDACTED**',
          'aws_secret_access_key': '**REDACTED**',
          'region': 'us-east-12'
        }
      }
    }
  }
}
