#!/usr/bin/env python

import argparse
import consulate
import logging

FORMAT = 'REDFIVE*** %(message)s'
logging.basicConfig(format=FORMAT, level=logging.INFO)
logger = logging.getLogger(__file__)

class ConfigService(object):
  CONSUL = 'consul'
  S3 = 's3' # Not implemented
  scope = ''

  def __init__(self, source):
    logger.info('ConfigService.__init__()')
    if source  == ConfigService.CONSUL:
      self.consul = consulate.Consul()
      self.source = self.consul.kv
    else:
      # TODO: throw an exception, strongly worded letter
      logger.warning('ConfigService setup without a valid source')

  def set_scope(self, scope):
    self.scope = scope

  def unset_scope(self):
    self.scope = ''

  def get(self, key):
    # TODO: better handling of bad setup
    logger.info('ConfigService: fetching key {}'.format(key))

    # strip out the protocol section if it is present
    # a real ConfigService might allow dynamic switch of sources between
    # things like s3, consul, memcache
    if '://' in key:
      key = key[key.find('://')+3:]
      logger.debug('ConfigService: key stripped to {}'.format(key))

    if self.scope:
      logger.debug('ConfigService: adding root scope {}'.format(self.scope + '.' + key))
      key = '{}'.format(self.scope + '.' + key)

    value = key
    if self.source:
      value = self.source.get(key)

    logger.debug('ConfigService: returning value {}'.format(value))
    return value

  def set(self, key, value):
    # TODO: handle bad setup
    if self.scope:
      key = self.scope + '.' + key
      logger.debug('ConfigService: scoping key {} to scope {}'.format(key, self.scope))
    logger.info('ConfigService: setting key {} to value {}'.format(key, value))
    return self.source.set(key, value)

  def set_all(self, config_obj, prefix=''):
    logger.info('set_all() -- prefix is: {}'.format(prefix))
    for o in config_obj:
      logger.debug('looking at object: {}'.format(o))
      if isinstance(config_obj[o], dict):
        self.set_all(config_obj[o], prefix + ('.' + o if len(prefix) else o) )
      elif isinstance(config_obj[o], basestring):
        key = prefix + ('.' + o if len(prefix) else o)
        value = config_obj[o]
        logger.debug('configuring prefix {} to {}'.format(key, value))
        self.set(key, value)
    return True

  def set_from_file(self, filepath):
    gv = lv = {}
    with open(filepath) as f:
      code = compile(f.read(), "config_file", "exec")
      exec(code, gv, lv)

    logger.info('config is: {}'.format(lv['config']))

    return self.set_all(lv['config'])

class ArgParseFactory(object):

  @classmethod
  def build_parser_from_file(cls, filepath):
    #
    # read in some parser configuration from a parser config file. 
    #   the contents will be used to initialize an ArgParse object
    #
    gv = lv = {}
    with open("parser_config.py") as f:
      code = compile(f.read(), "parser_config.py", "exec")
      exec(code, gv, lv)

    #logger.info('parser config is: {}'.format(lv['parser']))

    parser = cls.build_parser(lv['parser'])
    return parser

  @classmethod
  def build_parser(cls, config):
    cs = ConfigService(ConfigService.CONSUL)

    #
    # Handle the use of Consul to store configuration, in this case
    #   merely the version of the application.
    #
    version = cs.get(config['version'])
    logging.warning('parser version is: {}'.format(version))

    parser = argparse.ArgumentParser(version=version)

    if config['subparsers'] and len(config['subparsers']['parsers']) > 0:
      subparsers = parser.add_subparsers(help=config['subparsers']['help'],
                                         dest=config['subparsers']['name'])
      # add subparsers
      for sub in config['subparsers']['parsers']:
        cls.build_subparser(subparsers, sub)

    if len(config['args']) > 0:
      # add subparsers
      for a in config['args']:
        name = a.pop('name')
        parser.add_argument(name, **a)

    return parser

  @classmethod
  def build_subparser(cls, subparsers, parser):
    new_parser = subparsers.add_parser(parser['name'], help=parser['help'])

    if parser.has_key('subparsers') and len(parser['subparsers']['parsers']) > 0:
      new_subs = new_parser.add_subparsers(help=parser['subparsers']['help'],
                                           dest=parser['subparsers']['name'])

      for p in parser['subparsers']['parsers']:
        cls.build_subparser(new_subs, p)

    if parser.has_key('args') and len(parser['args']) > 0:
      for a in parser['args']:
        name = a.pop('name')
        new_parser.add_argument(name, **a)


def main():
  logger.info('in main')

  #parser = ArgParseFactory.build_parser_from_file('parse_config.py')
  #ar = parser.parse_args()

  cs = ConfigService(ConfigService.CONSUL)
  cs.set_from_file('test_config.py')

  # do something with the arguments parsed
  #logger.info('Parsed arguments: %s' % (ar))

if __name__ == '__main__':
  logger.info('at module scope')
  main()
